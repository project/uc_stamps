Issue #2179671 by M. George Hansen:  Fixed export/Import admin page occasionally whitescreens
Issue #2332757 by DanZ:  Fixed invalid CSV file error message with Stamps.com version 12

Ubercart Stamps.com 7.x-1.0-beta1
----------------------
Initial beta release.
